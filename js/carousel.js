/*global jQuery, document*/
(function ($) {
	 'use strict';

	 function Carousel($element) {
		this.$carousel = $element;
		this.options = this.$carousel.data();
		this.$slider = this.$carousel.find('.carousel-slide');
		this.$list = this.$slider.find('li');
		this.$textContainer = this.$carousel.find('.text-container');
		this.$playBtn = this.$carousel.find('#play-button');
		this.$thumbList = this.$carousel.find('.thumb-list li');
		this.autoPlay = this.options.autoplay || true;
		this.aniSpeed = this.options['ani-speed'] || 500;
		this.intervalTime = this.options['interval-time'] || 2500;
		this.imgWidth = this.$list.find('img').first().width();
		this.ACT_CLASS = 'active';
		
		this.init();
	 }

	 Carousel.prototype = {
		init: function () {
			this.moveFirstBeforeLast();
			this.updateList();
			this.updateText();
			this.highlightThumb();
			this.bindEvents();
			if (this.autoPlay) {
				this.play();
				this.updatePlayBtn('stop');
			}
		},
		bindEvents: function () {
			var self = this;

			this.$carousel.on('click', '.arrow', function () {
				self.stop();
				if ($(this).hasClass('next')) {
					self.animateRight();
				} else {
					self.animateLeft();
				}
			});

			this.$playBtn.on('click', function () {
				self.handlePlayBtn();
			});

			//Text description opacity
			self.$textContainer.hover(function () {
				self.animateOpacity($(this), 1);
			}, function () {
				self.animateOpacity($(this), 0.6);
			});

			//Thumbs opacity
			this.$thumbList.hover(function () {
				self.animateOpacity($(this), 0.6);
			}, function () {
				self.animateOpacity($(this), 1);
			});

			//Thumbs
			this.$thumbList.on('click', function () {
				self.slicePrepend($(this));
			});
		},
		play: function () {
			var self = this;
			
			this.isPlaying = true;
			this.intervalSlide = setInterval(function () {
				self.animateRight();
				self.highlightThumb();
			}, this.intervalTime);
		},
		stop: function () {
			clearInterval(this.intervalSlide);
			this.isPlaying = false;
			this.updatePlayBtn('play');
		},
		handlePlayBtn: function () {
			var value;
			if(this.isPlaying) {
				this.stop();
				value = 'play';
			} else {
				this.play();
				value = 'stop';
				
			}
			this.updatePlayBtn(value);
		},
		highlightThumb: function () {
			var currentIndex = this.getCurrentElement().data('index');

			this.$thumbList.removeClass(this.ACT_CLASS);
			this.$thumbList.filter('[data-index="' + currentIndex + '"]').addClass(this.ACT_CLASS);
		},
		updatePlayBtn: function (value) {
			var text = this.$playBtn.data(value);
			this.$playBtn.text(text);
		},
		animateRight: function () {
			var self = this,
				pos = this.getSliderLeft() - this.imgWidth,
				callback = function () {
					self.moveLastAfterFirst();
					self.updateSlide();
					self.highlightThumb();
				};

			this.animateSlider(pos, callback);
		},
		animateLeft: function () {
			var self = this,
				pos = this.getSliderLeft() + this.imgWidth,
				callback = function () {
					self.moveFirstBeforeLast();
					self.updateSlide();
					self.highlightThumb();
				};

			this.animateSlider(pos, callback);
		},
		animateOpacity: function ($el, value) {
			$el.stop().animate({opacity: value}, 'fast');
		},
		getSliderLeft: function () {
			return parseInt(this.$slider.css('left'), 10);
		},
		animateSlider: function (pos, callback) {
			this.$slider.stop().animate({'left': pos}, this.aniSpeed, callback);
		},
		updateSlide: function () {
			this.updateList();
			this.$slider.css({'left': -this.imgWidth});
			this.updateText();
		},
		updateList: function () {
			this.$list = this.$slider.find('li');
		},
		moveLastAfterFirst: function () {
			this.$list.last().after(this.$list.first());
		},
		moveFirstBeforeLast: function () {
			this.$list.first().before(this.$list.last());
		},
		updateText: function () {
			var currentImg = this.getCurrentElement().find('img'),
				altText = currentImg.attr('alt');

			this.$textContainer.find('p').text(altText);
		},
		slicePrepend: function ($element) {
			var thumbIndex = parseInt($element.data('index'), 10),
				currentIndex = parseInt(this.getCurrentElement().data('index'), 10),
				thumbIndexPosition,
				sliceList;
				
				this.stop();
				this.updateList();

			if (thumbIndex !== currentIndex) {
				if(thumbIndex !== 1) {
					thumbIndex -= 1;
				} else {
					thumbIndex = this.$list.size();
				}

				this.$list.hide();
				thumbIndexPosition = this.$list.filter("[data-index='"+ thumbIndex +"']").index();
				sliceList = this.$list.slice(thumbIndexPosition).remove();
				this.$slider.prepend(sliceList);
				this.$list.fadeIn();
				this.updateList();
				this.updateText();
				this.highlightThumb();
			}
		},
		getCurrentElement: function () {
			return this.$list.eq(1);
		}
	 };

	 $(document).ready(function () {
		var $element = $('.carousel'),
			carousel;

		if ($element.length) {
			carousel = new Carousel($element);
		}
	 });

}(jQuery));