/*global jQuery, document*/
(function ($) {
	'use strict';

	function Pagination($element) {
		this.$container = $element;
		this.$linkContainer = this.$container.find('.pagination-container');
		this.$review = this.$container.find('.reviews_list');
		this.$items = this.$review.find('li');
		this.$originalItems = this.$review.find('li');
		this.$pagPrev = this.$linkContainer.find('.arrow.prev');
		this.$pagNext = this.$linkContainer.find('.arrow.next');
		this.$pageList = this.$linkContainer.find('.page-links');
		this.currentPage = 0;
		this.DIS_CLASS = 'disabled';
		this.ACT_CLASS = 'active';

		this.init();
	}

	Pagination.prototype = {
		init: function () {
			this.hideAll();
			this.createPagination();
			this.showFirstItems();
			this.bindEvents();
		},
		bindEvents: function () {
			var self = this;
			//Next/Prev
			this.$linkContainer.on('click', '.arrow', function (e) {
				e.preventDefault();
				if ($(this).hasClass('prev')) {
					self.showPrev();
				} else {
					self.showNext();
				}
				
			});
			//Page Links
			this.$linkContainer.on('click', '.links a', function (e) {
				var index = parseInt($(this).text() - 1, 10);

				e.preventDefault();
				self.showPage(index);
			});
			//Sort
			this.$container.find('.sort').on('change', function() {
				var value = $(this).val();
				self.sortBy(value);
			});
		},
		hideAll: function () {
			this.$items.hide();
		},
		calculatePagination: function () {
			var totalItems = this.$items.size();

			this.itemPerPage = this.$container.data['items-per-page'] || 5;
			this.numPages = Math.ceil(totalItems/this.itemPerPage);
		},
		showFirstItems: function () {
			this.hideAll();
			this.$items.slice(0, this.itemPerPage).fadeIn();
			this.checkLinksStatus(this.currentPage);
		},
		showNext: function () {
			var nextPage;

			if (this.currentPage < (this.numPages - 1)) {
				nextPage = this.currentPage + 1;
				this.showPage(nextPage);
			}
		},
		showPrev: function () {
			var prevPage;

			if (this.currentPage > 0) {
				prevPage = this.currentPage - 1;
				this.showPage(prevPage);
			}
		},
		showPage: function(number) {
			var start,
				end;

			if (typeof number === 'number') {
				start = this.itemPerPage * number;
				end = start + this.itemPerPage;
				this.hideAll();
				this.$items.slice(start, end).show();
				this.currentPage = number;
				this.checkLinksStatus(number);
			}
		},
		createPagination: function () {
			var i = 1;
			this.calculatePagination();

			if (this.numPages > 1) {
				for (i; i <= this.numPages; i++) {
					this.$pageList.append('<li class="links"><a href="#">'+ i + '</a></li>');
				}
				this.$pageLinks = this.$pageList.find('li');
				//Enable Next/Prev links
				this.$linkContainer.addClass(this.ACT_CLASS);
			}
		},
		checkLinksStatus: function (pageNum) {
			this.$pagPrev.removeClass(this.DIS_CLASS);
			this.$pagNext.removeClass(this.DIS_CLASS);

			if (pageNum === 0) {
				this.$pagPrev.addClass(this.DIS_CLASS);
			} else if (pageNum === (this.numPages - 1)) {
				this.$pagNext.addClass(this.DIS_CLASS);
			}
			//Control exists only if pages > 1
			if (typeof this.$pageLinks !== 'undefined') {
				this.$pageLinks.removeClass(this.ACT_CLASS);
				this.$pageLinks.eq(pageNum).addClass(this.ACT_CLASS);
			}
		},
		sortBy: function (value) {
			switch (value) {
				case "default":
					this.$originalItems.appendTo(this.$review);
					break;
				case "up":
					this.$items.sort(this.sortUp).appendTo(this.$review);
					break;
				case "down":
					this.$items.sort(this.sortDown).appendTo(this.$review);
					break;
			}
			this.updateItemsList();
			this.showFirstItems();
			this.checkLinksStatus(0);
		},
		sortUp: function (a, b) {
			return ($(b).data('score')) < ($(a).data('score')) ? 1 : -1;    
		},
		sortDown: function (a, b) {
			return ($(b).data('score')) > ($(a).data('score')) ? 1 : -1;    
		},
		updateItemsList: function () {
			this.$items = this.$review.find('li');
		}
	};

	$(document).ready(function () {
		var $element = $('.reviews'),
			pagination;

		if ($element.length) {
			pagination = new Pagination($element);
		}
	});

}(jQuery));